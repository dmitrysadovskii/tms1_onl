import unittest
from main_task_18_1 import count_str


class TestString(unittest.TestCase):

    def test_1(self):
        self.assertEqual(count_str("aabbbcdd"), "a2b3cd2")

    def test_2(self):
        self.assertNotEqual(count_str("dddd"), "d")

    @unittest.expectedFailure
    def test_3(self):
        self.assertEqual(count_str("ccdddeee"), "c2")

    @unittest.expectedFailure
    def test_4(self):
        self.assertNotEqual(count_str("cc"), "c2")


if __name__ == "main":
    unittest.main()
