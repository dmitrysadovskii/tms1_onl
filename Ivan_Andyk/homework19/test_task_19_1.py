import pytest
from main_task_19_1 import count_str


@pytest.fixture
def first_test():
    print("Check before starting")


class TestString:

    @pytest.mark.smoke
    @pytest.mark.parametrize("param, result", [("dddcca", "d3c2a")])
    def test_str_1(self, param, result):
        assert count_str(param) == result

    @pytest.mark.xfail(reason="not in scope")
    def test_str_2(self, first_test):
        assert count_str("dddrrrcc") == "drc", "Answer: d3r3c2"
