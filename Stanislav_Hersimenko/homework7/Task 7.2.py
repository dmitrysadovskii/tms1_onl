# 1
def gen(numbers):

    for elem in numbers:
        if elem >= 0:
            yield elem


result = gen([34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7])
for b in result:
    print(b)

# 2
text = "the quick brown fox jumps over the lazy dog"
splitted_text = text.split()
List = [len(i) for i in splitted_text if i != "the"]
print(List)
