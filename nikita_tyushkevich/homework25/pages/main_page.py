from nikita_tyushkevich.homework25.pages.base_page import BasePage
from nikita_tyushkevich.homework25.locators.main_page_locators\
    import MainPageLocators


class MainPage(BasePage):

    def is_main_page(self):
        breadcrumb = self.find_elements(MainPageLocators.LOCATOR_BREADCRUMB)
        if not breadcrumb:
            pass

    def login_page(self):
        sign_in_button = self.find_element(MainPageLocators.LOCATOR_SIGN_IN)
        sign_in_button.click()

    def go_to_cart(self):
        cart_link = self.find_element(MainPageLocators.LOCATOR_CART)
        cart_link.click()
