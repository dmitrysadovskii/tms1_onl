number_names = {
    0: 'zero', 1: 'one', 2: 'two', 3: 'three', 4: 'four',
    5: 'five', 6: 'six', 7: 'seven', 8: 'eight', 9: 'nine',
    10: 'ten', 11: 'eleven', 12: 'twelve', 13: 'thirteen', 14: 'fourteen',
    15: 'fifteen', 16: 'sixteen', 17: 'seventeen', 18: 'eighteen',
    19: 'nineteen'
}


def decorator(func):

    def wrapper(*args):

        numbers_list = []
        for i in args:
            if i in number_names:
                numbers_list.append(number_names[i])
        # using sorted to get alphabetic order for list values
        sorted_list = sorted(numbers_list)

        # Check myself how sorted_list looks like :)
        print(sorted_list)

        # Convert words back to numbers preserving the order
        final_output = []
        for j in sorted_list:
            for key, value in number_names.items():
                if j == value:
                    final_output.append(key)
        return final_output
    return wrapper


@decorator
def text_output(*args):
    print(*args)


print(text_output(1, 7, 2, 4, 5, 6, 7, 8, 9, 15))
