import pytest
from bulls_cows import game_part


@pytest.fixture(autouse=True)
def preparation():
    print("New case")


@pytest.mark.smoke
class TestStrings:
    def test_positive_one(self):
        assert game_part([1, 2, 3, 4], 1234) == (4, 4)


@pytest.mark.parametrize("pc, u_input", [([1, 2, 3, 4], 1234)])
class TestExperiment:
    def test_positive_two(self, pc, u_input):
        assert game_part(pc, u_input) == (4, 4)


@pytest.mark.xfail(reason="using duplicate numbers shouldn't be allowed")
class TestFailed:
    def test_expect_failure(self):
        assert game_part([9, 7, 5, 2], 9772) == (3, 3)
