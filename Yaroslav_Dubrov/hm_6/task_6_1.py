card_num = input("Enter your card number: ")


def validate(card_num: int):

    # Convert card number to list and find its length
    list_card_num = [int(x) for x in str(card_num)]
    length = len(list_card_num)
    check_sum = 0

    # Create iterable object to allow check each digit in list_card_num
    for y, z in enumerate(list_card_num):

        # if card number contains an even number of digits: (even y * 2)
        if length % 2 == 0:
            if y % 2 == 0:
                temp = z * 2
                if temp > 9:
                    temp -= 9
                check_sum += temp
            else:
                check_sum += z

        # if card number contains an odd number of digits: (odd y * 2)
        else:
            if y % 2 != 0:
                temp = z * 2
                if temp > 9:
                    temp -= 9
                check_sum += temp
            else:
                check_sum += z

    return (check_sum % 10) == 0


# Finally check if something wrong was entered instead of the card number
if card_num.isnumeric():
    print(validate(card_num))
else:
    print("You entered wrong number")
