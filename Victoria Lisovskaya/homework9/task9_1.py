class Book:
    def __init__(self, name: str, author: str, pages: int,
                 isbn, reserved=False):
        self.name = name
        self.author = author
        self.pages = pages
        self.isbn = isbn
        self.reserved = reserved


class User:
    # список всех юзеров
    all_users = []

    def __init__(self, name: str):
        self.name = name
        self.taken_books = []
        self.reserved_books = []
        User.all_users.append(self)

    def take_book(self, book):
        # флажок, который показывает что юзер может взять книгу
        is_success = True
        for user in User.all_users:
            # проверяем, что книга не взята кем-то
            if book in user.taken_books:
                print(f"You can't take {book.name}. It is already taken.")
                is_success = False
            # проверяем, что книга не зарезервирована другим юзером
            elif book in user.reserved_books and user != self:
                print(f"{book.name} is already reserved!")
                is_success = False
        if is_success:
            self.taken_books.append(book)
            # если книга была зарезервирована, то снимаем ее из резервирования
            if book.reserved:
                book.reserved = False
            print(f"{self.name} takes {book.name}")

    def return_book(self, book):
        self.taken_books.remove(book)
        print(f"{book.name} is returned.")

    def reserve_book(self, book):
        # если книга НЕ зарезервирована, то юзер резервирует ее
        if not book.reserved:
            self.reserved_books.append(book)
            book.reserved = True
            print(f"{book.name}  is successfully reserved.")
        # если книга зарезервирована, то предупреждаем об этом юзера
        else:
            print(f"{book.name} is already reserved!")


book1 = Book("Book1", "Tolkien", 1023, "1")
book2 = Book("Book2", "Bronte", 899, "2")
book3 = Book("Book3", "Carroll", 500, "3")

user1 = User("Ivanov")
user2 = User("Petrov")
user1.take_book(book1)
user1.return_book(book1)
user1.take_book(book2)
user1.reserve_book(book3)
user2.take_book(book1)
user2.take_book(book2)
user2.take_book(book3)
user2.reserve_book(book3)
user1.take_book(book3)
