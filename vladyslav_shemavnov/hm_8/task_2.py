number_names = {
    0: 'zero', 1: 'one', 2: 'two', 3: 'three',
    4: 'four', 5: 'five', 6: 'six', 7: 'seven',
    8: 'eight', 9: 'nine', 10: 'ten', 11: 'eleven',
    12: 'twelve', 13: 'thirteen', 14: 'fourteen',
    15: 'fifteen', 16: 'sixteen', 17: 'seventeen',
    18: 'eighteen', 19: 'nineteen'
}


def number_decorator(func):
    def wrapper(input_numbers: str):
        # создаю список цифр разбивая через пробел
        list_numbers = input_numbers.split(" ")
        # список строковых значений цифр
        words = []
        for k, v in number_names.items():
            for number in list_numbers:
                # если введенное число совпадает
                # с ключом из списка,
                # тогда добавляю его в список words
                if number == str(k):
                    words.append(v)
        # создаю новый список с отсортированными словами
        sorted_words = sorted(words)
        func(sorted_words)
    return wrapper


@number_decorator
def return_sorted_string_numbers(input_numbers):
    print(input_numbers)


input_numbers = input("Введите числа от 0 до 19 "
                      "через пробел:")
return_sorted_string_numbers(input_numbers)
