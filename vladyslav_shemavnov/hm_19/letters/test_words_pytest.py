import pytest
from letters_numbers import words


@pytest.fixture(scope="function")
def my_fixture():
    print("I am your fixture")


class TestMyClass(object):

    @pytest.mark.parametrize("number, correct_answer",
                             [("aaabbvb", "a3b2vb"),
                              ("abeehhhhhccced", "abe2h5c3ed")])
    def test_success(self, number, correct_answer, my_fixture):
        assert words(number), correct_answer

    @pytest.mark.xfail(reason="using wrong data")
    def test_failed(self, my_fixture):
        assert words("aaabbvb"), "a5b3vb"
