card = input("Enter your credit card number here: ")


def func():
    sum_numbers = 0
    for i, numbers in enumerate(reversed(card)):
        n = int(numbers)
        if i % 2 == 0:
            sum_numbers += n
        elif n >= 5:
            sum_numbers += n * 2 - 9
        else:
            sum_numbers += n * 2
    if sum_numbers % 10 == 0:
        print("True!")
    else:
        print("False!")


func()
