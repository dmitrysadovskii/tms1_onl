"""работа с yaml файлом
* Напечатайте номер заказа
* Напечатайте адрес отправки
* Напечатайте описание посылки, ее стоимость и кол-во
* Сконвертируйте yaml файл в json
* Создайте свой yaml файл
"""

import yaml
import json

with open('order.yaml', 'r') as f:
    data = yaml.safe_load(f)

# Напечатать номер заказа и адрес
print("Номер заказа:", data["invoice"])
print("Адрес отправки:", data["bill-to"]["address"])

# Напечать описание посылки, ее стоимость и кол-во
for el in data['product']:
    quantity = el['quantity']
    description = el['description']
    price = el['price']
    print(f'Количество - {quantity}, описание - {description}, цена - {price}')

# Конвертирую yaml файл в json
with open("create_json.json", "w") as json_file:
    json.dump(str(data), json_file)

# Создаю свой yaml файл
first_data = ['Dina Shunkina']
second_data = ['I am QA Engineer']
yaml_file = {'file1': first_data, 'file2': second_data}

with open('new_order.yaml', 'w') as f:
    yaml.dump(yaml_file, f)
