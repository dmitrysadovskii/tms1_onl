from pages.base_page import BasePage
from locators.Main_Page_Locators import MainPageLocators


class MainPage(BasePage):

    def should_be_main_page(self):
        best_sellers_text = self.find_element(
            MainPageLocators.LOCATOR_BEST_SELLERS_BUTTON).text
        assert best_sellers_text == "BEST SELLERS",\
            f'BEST SELLERS not eq {best_sellers_text}'

    def open_login_page(self):
        self.find_element(MainPageLocators.LOCATOR_SIGN_IN_BUTTON).click()

    def open_cart_page(self):
        self.find_element(MainPageLocators.LOCATOR_GO_TO_CART_PAGE).click()

    def switch_between_tabs(self, locator):
        self.find_element(locator).click()

    def should_be_women_tab(self):
        women_text = self.find_element(
            MainPageLocators.LOCATOR_WOMEN_TAB_TEXT).text
        assert women_text == "Women", f'Women not eq {women_text}'

    def should_be_dresses_tab(self):
        dresses_text = self.find_element(
            MainPageLocators.LOCATOR_DRESSES_TAB_TEXT).text
        assert dresses_text == "Dresses", f'Dresses not eq {dresses_text}'

    def should_be_tshirts_tab(self):
        tshirts_text = self.find_element(
            MainPageLocators.LOCATOR_TSHIRTS_TAB_TEXT).text
        assert tshirts_text == "T-shirts",\
            f'T-shirts not eq {tshirts_text}'

    def add_item_to_cart(self):
        self.hover_element(MainPageLocators.LOCATOR_PRINTED_DRESS)
        self.find_element(MainPageLocators.LOCATOR_ADD_TO_CART).click()
        self.find_element(MainPageLocators.LOCATOR_CONTINUE_SHOPPING).click()
