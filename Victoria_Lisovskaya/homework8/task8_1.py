def typed(type1):
    def decorator(func):
        def wrapper(*args):
            new_args = []
            if type1 == "str":
                new_args = list(map(str, args))
            elif type1 == "int":
                new_args = map(int, args)
            elif type1 == "float":
                new_args = map(float, args)
            return func(*new_args)
        return wrapper
    return decorator


@typed(type1="str")
def add_two_symbols(a, b):
    return a + b


print(add_two_symbols("3", 5))
print(add_two_symbols(5, 5))
print(add_two_symbols('a', 'b'))


@typed(type1="int")
def add_three_symbols(a, b, c):
    return a + b + c


print(add_three_symbols(5, 6, 7))
print(add_three_symbols("3", 5, 0))


@typed(type1="float")
def add_three_symbols(a, b, c):
    return a + b + c


print(add_three_symbols(0.1, 0.2, 0.4))
