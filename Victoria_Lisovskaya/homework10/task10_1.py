class Flower:
    def __init__(self, name: str, price: int, lifetime: int,
                 colour: str, length: int):
        self.name = name
        self.price = price
        self.lifetime = lifetime
        self.colour = colour
        self.length = length


class Accessory:
    def __init__(self, name: str, price: int):
        self.name = name
        self.price = price


class Bouquet:
    def __init__(self, name: str, price=0):
        self.name = name
        self.price = price
        self.items = []

    def add_item(self, item):
        self.items.append(item)
        self.price += item.price
        print(f"{item.name} was added to bouquet {self.name}")
        print(f"Bouquet {self.name} price is {self.price}$")

    def calc_price(self):
        price = 0
        for item in self.items:
            price += item.price

        print(f"Bouquet {self.name} price is {price}$")

    def calc_lifetime(self):
        lifetime = 0
        k = 0
        for item in self.items:
            if hasattr(item, 'lifetime'):
                lifetime += item.lifetime
                k += 1
        if k != 0:
            lifetime = lifetime / k
            print(f"Bouquet lifetime is {lifetime} days")
        else:
            print("Your bouquet is eternal or has no items")

    def find_flower(self, param: str, value):
        count = 0
        for item in self.items:
            if getattr(item, param) == value:
                count += 1
                print(f"Flower {item.name} has {param}  {value}")
        if count == 0:
            print(f"Flower with {param} {value} wasn't founded")

    def sort_flower(self, param: str):
        sorted_list = sorted(self.items, key=lambda x: getattr(x, param))
        names_list = [item.name for item in sorted_list]
        print(f"Flowers are sorted by {param}: {names_list}")


# flowers
rose = Flower("rose", 10, 3, "red", 25)
camomile = Flower("camomile", 5, 5, "white", 40)
peony = Flower("peony", 50, 4, "pink", 30)
# accessory
tape = Accessory("tape", 5)
paper = Accessory("paper", 10)
# bouquet
bouquet1 = Bouquet("Wedding")
bouquet1.add_item(rose)
bouquet1.add_item(camomile)
bouquet1.add_item(peony)
bouquet1.add_item(tape)
bouquet1.add_item(paper)
bouquet1.calc_price()
bouquet1.calc_lifetime()
bouquet1.find_flower("name", "peony")
bouquet1.find_flower("price", 5898)
bouquet1.sort_flower("name")
