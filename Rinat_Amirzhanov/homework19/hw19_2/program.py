game = True


def game_bulls(comp_number, player_number):
    bulls = 0
    cows = 0
    splitted_input = [int(i) for i in str(comp_number)]
    splitted_player_input = [int(i) for i in str(player_number)]
    for index, elem in enumerate(splitted_input):
        player_elem = splitted_player_input[index]
        if elem == player_elem:
            bulls = bulls + 1
        elif elem in splitted_player_input:
            cows = cows + 1
    if bulls == 4:
        return "Вы выйграли!"
    else:
        print("Быки: " + str(bulls), "Коровы: " + str(cows))
    return [bulls, cows]
