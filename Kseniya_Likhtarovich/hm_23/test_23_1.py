"""Нажмите на 2-ю сверху кнопку во втором столбце."""


def test_button(browser):
    xp = "//a[@class='et_pb_button et_pb_button_4 et_pb_bg_layout_light']"
    css = ".et_pb_button.et_pb_button_4"
    cl_name = "et_pb_button_4"

    browser.get("https://ultimateqa.com/complicated-page/")

    xp_el = browser.find_element_by_xpath(xp)
    xp_el.click()

    css_el = browser.find_element_by_css_selector(css)
    css_el.click()

    cl_name_el = browser.find_element_by_class_name(cl_name)
    cl_name_el.click()
